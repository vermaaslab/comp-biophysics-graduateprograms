# Comp Biophysics Graduate Programs

This is Josh's not-at-all exhaustive list of good biophysics programs around the country, intended to be useful to senior undergraduate students looking to do computational biophysics as a PhD.
Josh has assembled the list with minimal effort, mostly based on memory of cool talks he has heard over the years and vaguely remembering where folks are now.
To be on this list, there are a few requirements.

- The graduate program should have rotations prior to choosing a PhD advisor (I link to the website where this is clear)
- Have at least a handful of cool people doing computational biophysics research (I link to a page that *hopefully* gets you to relevant information. This list is not meant to be highly curated. This is meant to be useful to my students.)

## Midwest

* University of Illinois at Urbana-Champaign, [Biophysics](https://biophysics.illinois.edu/program/courses)
    - [Emad Tajkhorshid](https://biophysics.illinois.edu/people/emad)
    - [Diwakar Shukla](https://biophysics.illinois.edu/people/diwakar)
    - [Taras Pogorelov](https://biophysics.illinois.edu/people/pogorelo)
    - [Zan Schulten](https://biophysics.illinois.edu/people/zan)
    - [Alek Aksimentiev](https://biophysics.illinois.edu/people/aksiment)

* University of Chicago, [Biophysics](https://biophysics.uchicago.edu/the-program/), or [Biochemistry and Molecular Biophysics](https://bcmb.uchicago.edu/page/first-year-1)
    - [Aaron Dinner](https://biophysics.uchicago.edu/the-faculty/aaron_dinner/)
    - [Andrew Ferguson](https://biophysics.uchicago.edu/the-faculty/andrew_ferguson/)
    - [Benoit Roux](https://biophysics.uchicago.edu/the-faculty/benoit_roux/)
    - [Tobin Sosnick](https://biophysics.uchicago.edu/the-faculty/tobin_sosnick/)
    - [Greg Voth](https://biophysics.uchicago.edu/the-faculty/gregory_a_voth/)

* Michigan State University, [Biomolecular Science](https://biomolecular.natsci.msu.edu/applicants/program-requirements.aspx)
    - [Alex Dickson](https://directory.natsci.msu.edu/Directory/Profiles/Person/100331?org=6&group=262)
    - [Michael Feig](https://directory.natsci.msu.edu/Directory/Profiles/Person/100347?org=6&group=262)
    - [Kenny Merz](https://directory.natsci.msu.edu/Directory/Profiles/Person/100329?org=6&group=262)
    - [Josh Vermaas](https://directory.natsci.msu.edu/Directory/Profiles/Person/100419?org=6&group=262)

* University of Michigan, [Biophysics](https://lsa.umich.edu/biophysics/graduates/program-details.html)
    - [Charlie Brooks](https://lsa.umich.edu/biophysics/people/core-faculty/brookscl.html)
    - [Sharon Glotzer](https://glotzerlab.engin.umich.edu/home/)

## The East

* University of Maryland, [Biophysics](https://ipst.umd.edu/graduate-programs/biophysics/prospective-students/admissions) or [Chemistry and Biochemistry](https://chem.umd.edu/graduate-program/current-students/biochemistry-requirements-and-forms)
    - [Pratyush Tiwary](https://chem.umd.edu/people/pratyush-tiwary)
    - [Yanxin Liu](https://chem.umd.edu/people/yanxin-liu)
    - [Jeff Klauda](https://user.eng.umd.edu/~jbklauda/index.html)

* University of Rochester, [Biophysics, Structrual, and Computational Biology](https://www.urmc.rochester.edu/education/graduate/phd/biophysics/curriculum.aspx)
    - [Alan Grossfield](https://www.urmc.rochester.edu/people/27117323-alan-m-grossfield)
    - [Andrew White](http://www.hajim.rochester.edu/che/people/faculty/white_andrew/index.html)

* Stony Brook University, [Biochemistry and Structural Biology](https://www.stonybrook.edu/commcms/bsb/phd-program/)
    - [Ivet Bahar](http://www.bahargroup.org/Faculty/bahar/)
    - [Robert Rizzo](https://ringo.ams.stonybrook.edu/~rizzo/StonyBrook/index.html)
    - [Carlos Simmerling](https://www.simmerlinglab.org/)
    - [Ken Dill](https://dillgroup.org/#/research)


* Tri-Institutional PhD Program - Weil Cornell Medicine, Memorial Sloan Kettering Cancer Center, The Rockefeller University, [Computational Biology & Medicine](https://compbio.triiprograms.org/)
    - [John Chodera](https://www.choderalab.org/)
    - [Harel Weinstein](https://physiology.med.cornell.edu/research/weinsteinlab/)
    - [George Khelashvili](https://physiology.med.cornell.edu/faculty/khelashvili/lab/)

## The West

* University of California San Diego, [Biochemistry and Molecular Biophysics](https://www-chem.ucsd.edu/graduate-program/bmb/bmb-degree-reqs/index.html)
    - [Rommie Amaro](https://chemistry.ucsd.edu/faculty/profiles/amaro_rommie_e.html)
    - [Michael Gilson](https://chemistry.ucsd.edu/faculty/profiles/gilson_michael_k.html)
    - [Andy Maccammon](https://chemistry.ucsd.edu/faculty/profiles/mccammon_james_a.html)

* University of California Irvine, [Structural Biology, Biochemistry, and Biophysics](https://cmb.uci.edu/structural-biology-biochemistry-and-biophysics/)
    - [Ray Luo](https://www.faculty.uci.edu/profile/?facultyId=4618)
    - [Alvin Yu](https://yu-comp-bio.web.app/)

* University of California Los Angeles, [Biochemistry, Molecular and Structural Biology](https://grad.ucla.edu/programs/physical-sciences/chemistry-and-biochemistry-department/biochemistry-molecular-and-structural-biology/)
    - [Steffen Lindert](https://research.cbc.osu.edu/lindert.1/)
    - [James Bowie](https://bowielab.mbi.ucla.edu/research-2/)
    

* University of California San Francisco,[Biophysics](https://biophysics.ucsf.edu/degree-program/path-to-phd)
    - [Tanja Kortemme](http://kortemmelab.ucsf.edu/)
    - [Andrej Sali](http://www.salilab.org/)
    - [Bill DeGrado](http://www.degradolab.org/)
    - [Michael Grabe](http://www.cvri.ucsf.edu/~grabe/)

* University of Washington-Seattle,[Biochemistry](https://sites.uw.edu/biochemistry/biochemistry-graduate-program/)
    - [Valeria Daggett](https://sites.uw.edu/daggett-lab/)
    - [Neil King](https://kinglab.ipd.uw.edu/)
    - [David Baker](https://www.bakerlab.org/)

* Arizona State, [Biochemistry](https://sms.asu.edu/degree/graduate/phd-biochemistry-phd) or [Physics](https://physics.asu.edu/degree/graduate/phd-physics--phd), which both feed into the [Center for Biological Physics](https://cbp.asu.edu/content/graduate-opportunities), which lists youtube videos for multiple labs.
    - Abhishek Singharoy
    - Matthias Heyden
    - Oliver Beckstein
    - Petr Sulc
    - Steve Presse
    - Banu Ozkan

* Oregon State University, [Biochemistry and Biophysics](https://biochem.oregonstate.edu/graduate/academics/core-curriculum)
    - [Juan Vanegas](https://biochem.oregonstate.edu/directory/juan-vanegas)
